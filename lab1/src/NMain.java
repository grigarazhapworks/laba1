public class NMain {
	//Объявляем поле класса типа String, которое будет хранить вывод
    public String output;
	//Конструктор класса NMain со строковым параметром out, который хранит вывод
    public NMain(String out){
        output = out;
    }
	//Метод viv, который выводит содержание поля output
    public void viv(){
        System.out.println(output);
    }
	//Перегрузка метода viv, который выводит строковый параметр out.
    public void viv(String out){
        System.out.println(out);
    }
}
