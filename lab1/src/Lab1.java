public class Lab1 {
    public static void main(String[] args){
		//Создаем экземпляр класса NMain
        NMain nMain = new NMain("Message 1");
		//Вызываем метод viv без параметров
        nMain.viv();
		//Вызываем метод viv со строковым параметром
        nMain.viv("Message 2");
    }
}
